package br.com.cscbrasil.common;

import com.marzapower.loggable.Log;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public final class Parameters {

    private static Map<String, String> parameter_cache;
    private static String configFileName = "config.properties";

    public static int getInt(String identificador, String alias) throws IOException, FileNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NumberFormatException {
        String t = get(identificador, alias);
        if (t == null) {
            t = "0";
        }
        if (t.length() == 0) {
            t = "0";
        }

        int retorno;
        retorno = Integer.parseInt(t);
        return retorno;
    }

    public static long getLong(String identificador, String alias) throws IOException, FileNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NumberFormatException {
        String t = get(identificador, alias);
        if (t == null) {
            t = "vazio";
        }
        if (t.length() == 0) {
            t = "vazio";
        }

        long retorno;
        retorno = Long.parseLong(t);
        return retorno;
    }

    private Parameters() {
        // Nao pode ser instanciada !
    }

    public static void setConfigFileName(String newFilename) {
        boolean mkdir = new File("conf").mkdir();
        configFileName = String.format("conf/%s.properties", newFilename);
        File t = new File(configFileName);
        Log.get().info(String.format("Arquivo de configuracao: %s", t.getAbsoluteFile()));
    }

    public static String encrypt(String plaintext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NumberFormatException {
        byte[] keyData = {00, 54, 67, 48, 12, 53};
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
        Cipher cipher;
        cipher = Cipher.getInstance("Blowfish");

        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        byte[] hasil;
        hasil = cipher.doFinal(plaintext.getBytes());

        return Base64.encodeBase64String(hasil);
    }

    public static String decrypt(String string) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NumberFormatException {
        byte[] keyData = {00, 54, 67, 48, 12, 53};
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
        Cipher cipher;
        cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);

        byte[] hasil;
        hasil = cipher.doFinal(Base64.decodeBase64(string));
        String retorno = new String(hasil);
        return retorno;
    }

    private synchronized static void loadParameterCache() throws IOException, FileNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NumberFormatException {
        loadParameterCache(false);
    }

    private synchronized static void loadParameterCache(boolean reload) throws FileNotFoundException, IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NumberFormatException {
        if (parameter_cache != null && !reload) {
            return;
        }

        parameter_cache = new HashMap<>();

        File propertiesFile = new File(configFileName);
        if (!propertiesFile.exists()) {
            createProperties(propertiesFile);
        }

        InputStream input;
        Properties prop = new Properties();

        input = new FileInputStream(configFileName);

        prop.load(input);

        for (final java.util.Map.Entry<Object, Object> entry : prop.entrySet()) {
            String chave = (String) entry.getKey();
            String valor = (String) entry.getValue();
            if (chave.endsWith("password")) {
                valor = decrypt(valor);
            }
            parameter_cache.put(chave, valor);
        }

        input.close();

        String pass = get("fonte", "password");
        if (pass.isEmpty()) {
            pass = readPassword("fonte.password");
            set("fonte.password", pass);
        }
        pass = get("destino", "password");
        if (pass.isEmpty()) {
            pass = readPassword("destino.password");
            set("destino.password", pass);
        }
    }

    public static String readPassword(String chave) {
        Console console = System.console();
        if (console == null) {
            System.out.println("Couldn't get Console instance");
            System.exit(0);
        }
        //console.printf("Testing password%n");
        char passwordArray[] = console.readPassword("Entre a senha para " + chave + ":");
        return new String(passwordArray);
    }

    public static String get(String alias) throws IOException, FileNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NumberFormatException {
        return get("remedy", alias);
    }

    private synchronized static String cacheGet(String alias) throws IOException, FileNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NumberFormatException {
        if (parameter_cache == null) {
            loadParameterCache();
        }
        String retorno = parameter_cache.get(alias);
        if (retorno == null) {
            return "";
        }
        return retorno;
    }

    @SuppressWarnings("SynchronizeOnNonFinalField")
    public static String get(String identificador, String alias) throws IOException, FileNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NumberFormatException {
        String aliasUser = "user";
        if (!identificador.isEmpty()) {
            identificador += ".";
            alias = identificador.concat(alias);
            aliasUser = identificador.concat("user");
        }
        alias = alias.toLowerCase();
        aliasUser = aliasUser.toLowerCase();

        String retorno = cacheGet(alias);
        if (alias.endsWith("password") && !retorno.isEmpty()) {
            String user = cacheGet(aliasUser);
            if (user.equals("Remedy Application Service")) {
                retorno = "R!APp@Svc#" + new StringBuilder(retorno).reverse().toString();
            }
            if (user.equals("MidTier Service")) {
                retorno = "Mid-Tier!#Service!" + new StringBuilder(retorno).reverse().toString();
            }
        }

        if (retorno == null) {
            retorno = "";
        }

        return retorno;
    }

    public static void set(String alias, String valor) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NumberFormatException, FileNotFoundException, IOException {
        alias = alias.toLowerCase();

        if (alias.endsWith("password")) {
            valor = encrypt(valor);
        }

        File propertiesFile = new File(configFileName);
        if (!propertiesFile.exists()) {
            createProperties(propertiesFile);
        }

        InputStream input;
        Properties prop = new Properties();

        input = new FileInputStream(configFileName);

        prop.load(input);

        OutputStream output;
        output = new FileOutputStream(configFileName);
        prop.setProperty(alias, valor);
        prop.store(output, null);
        output.close();

        loadParameterCache(true);
    }

    public static void createProperties(File propertiesFile) throws FileNotFoundException, IOException {
        Properties prop = new Properties();
        OutputStream output;

        output = new FileOutputStream(propertiesFile);

        prop.setProperty("fonte.user", "Remedy Application Service");
        prop.setProperty("fonte.password", "");
        prop.setProperty("fonte.auth", "");
        prop.setProperty("fonte.locale", "en_US");
        prop.setProperty("fonte.hostname", "");
        prop.setProperty("fonte.port", "0");
        prop.setProperty("fonte.rpc", "390666");

        prop.setProperty("destino.user", "Remedy Application Service");
        prop.setProperty("destino.password", "");
        prop.setProperty("destino.auth", "");
        prop.setProperty("destino.locale", "en_US");
        prop.setProperty("destino.hostname", "");
        prop.setProperty("destino.port", "0");
        prop.setProperty("destino.rpc", "390666");

        prop.setProperty("syncreport.min_sleep_1", "1000");
        prop.setProperty("syncreport.min_sleep_2", "2000");
        prop.setProperty("syncreport.index_threshold", "0");
        prop.setProperty("syncreport.updated_threshold", "60");
        prop.setProperty("syncreport.intervalo_minutos_init", "2");
        prop.setProperty("syncreport.intervalo_minutos_retry", "1");
        prop.setProperty("syncreport.intervalo_minutos_sync_defs", "1");
        prop.setProperty("syncreport.intervalo_minutos_deleter", "1");
        prop.setProperty("syncreport.intervalo_minutos_fixne", "1");
        prop.setProperty("syncreport.intervalo_minutos_disable_filters", "1");
        prop.setProperty("syncreport.intervalo_horas_manter_comando_apagar", "1");
        prop.setProperty("syncreport.hora_executar_expurgo", "14");
        prop.setProperty("syncreport.max_records", "2000");
        prop.setProperty("syncreport.num_records_searchstring_db", "1");

        prop.store(output, null);

        output.close();
    }
}
