/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.bmc;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.OutputInteger;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.SortInfo;
import com.bmc.cmdb.api.CMDBClass;
import com.bmc.cmdb.api.CMDBClassNameKey;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ivan
 */
public class CmdbInfo {

    private boolean CMDB;
    private String className;
    private String nameSpace;
    private final String formName;
    private CMDBClassNameKey classKey;
    private CMDBClass cmdbClass;

    public CmdbInfo(ARServerUser ar, String formName) {
        this.formName = formName.trim();
        this.CMDB = false;
        className = null;
        nameSpace = null;
        classKey = null;
        cmdbClass = null;
        if (formName.contains("\"")) {
            return;
        }
        if (formName.endsWith("_")) {
            formName = new String(formName.substring(0, formName.length() - 1));
        }

        int[] fieldIds = {400130800, 400109900, 490001100};
        QualifierInfo qualification;
        try {
            qualification = ar.parseQualification("OBJSTR:Class", String.format("'400130800'=\"%s\"", Statics.escapeForDatabase(formName)));
        } catch (ARException ex) {
            // nem tem o form OBJSTR            
            return;
        }
        ArrayList<SortInfo> sortList = new ArrayList<>();
        sortList.add(new SortInfo(1, Constants.AR_SORT_ASCENDING));
        OutputInteger nMatches = new OutputInteger();
        List<Entry> result;
        try {
            result = ar.getListEntryObjects("OBJSTR:Class", qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, sortList, fieldIds, false, nMatches);
        } catch (ARException ex) {
            Log.get().fatal(ex.getMessage(), ex);
            return;
        }
        for (Entry record : result) {
            if (record.get(400109900).getValue() != null) {
                this.nameSpace = record.get(400109900).toString();
            }
            if (record.get(490001100).getValue() != null) {
                this.className = record.get(490001100).toString();
            }
            this.CMDB = (this.className != null && this.nameSpace != null);
            break;
        }

        if (this.CMDB) {
            this.classKey = new CMDBClassNameKey(this.className, this.nameSpace);
            try {
                this.cmdbClass = CMDBClass.findByKey(ar, this.classKey, true, true);
            } catch (ARException ex) {
                Log.get().error(ex.getMessage(), ex);
                this.CMDB = false;
                className = null;
                nameSpace = null;
                classKey = null;
                cmdbClass = null;
            }
        }
    }

    public boolean isCMDB() {
        return CMDB;
    }

    public String getClassName() {
        return className;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public String getFormName() {
        return formName;
    }

    public CMDBClassNameKey getClassKey() {
        return classKey;
    }

    public void setClassKey(CMDBClassNameKey classKey) {
        this.classKey = classKey;
    }

    public CMDBClass getCmdbClass() {
        return cmdbClass;
    }

    public void setCmdbClass(CMDBClass cmdbClass) {
        this.cmdbClass = cmdbClass;
    }
}
