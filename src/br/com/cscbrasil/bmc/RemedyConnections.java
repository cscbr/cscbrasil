/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.bmc;

import br.com.cscbrasil.common.Parameters;
import com.marzapower.loggable.Log;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author ivan
 */
public class RemedyConnections {

    public static String fonte_user;
    public static String fonte_password;
    public static String fonte_auth;
    public static String fonte_locale;
    public static String fonte_hostname;
    public static int fonte_port;
    public static int fonte_rpc;

    public static String fonte_adm_user;
    public static String fonte_adm_password;
    public static String fonte_adm_auth;
    public static String fonte_adm_locale;
    public static String fonte_adm_hostname;
    public static int fonte_adm_port;
    public static int fonte_adm_rpc;

    public static String destino_user;
    public static String destino_password;
    public static String destino_auth;
    public static String destino_locale;
    public static String destino_hostname;
    public static int destino_port;
    public static int destino_rpc;

    public static String destino_adm_user;
    public static String destino_adm_password;
    public static String destino_adm_auth;
    public static String destino_adm_locale;
    public static String destino_adm_hostname;
    public static int destino_adm_port;
    public static int destino_adm_rpc;

    static HashMap<String, RemedyConnection> map = new HashMap<>();

    public static void init() throws IOException, FileNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NumberFormatException {
        fonte_user = Parameters.get("fonte", "user");
        fonte_password = Parameters.get("fonte", "password");
        fonte_auth = Parameters.get("fonte", "auth");
        fonte_locale = Parameters.get("fonte", "locale");
        fonte_hostname = Parameters.get("fonte", "hostname");
        fonte_port = Parameters.getInt("fonte", "port");
        fonte_rpc = Parameters.getInt("fonte", "rpc");

        destino_user = Parameters.get("destino", "user");
        destino_password = Parameters.get("destino", "password");
        destino_auth = Parameters.get("destino", "auth");
        destino_locale = Parameters.get("destino", "locale");
        destino_hostname = Parameters.get("destino", "hostname");
        destino_port = Parameters.getInt("destino", "port");
        destino_rpc = Parameters.getInt("destino", "rpc");

        /*
        fonte_adm_user = Parameters.get("fonte_adm", "user");
        fonte_adm_password = Parameters.get("fonte_adm", "password");
        fonte_adm_auth = Parameters.get("fonte_adm", "auth");
        fonte_adm_locale = Parameters.get("fonte_adm", "locale");
        fonte_adm_hostname = Parameters.get("fonte_adm", "hostname");
        fonte_adm_port = Parameters.getInt("fonte_adm", "port");
        fonte_adm_rpc = Parameters.getInt("fonte_adm", "rpc");

        destino_adm_user = Parameters.get("destino_adm", "user");
        destino_adm_password = Parameters.get("destino_adm", "password");
        destino_adm_auth = Parameters.get("destino_adm", "auth");
        destino_adm_locale = Parameters.get("destino_adm", "locale");
        destino_adm_hostname = Parameters.get("destino_adm", "hostname");
        destino_adm_port = Parameters.getInt("destino_adm", "port");
        destino_adm_rpc = Parameters.getInt("destino_adm", "rpc");
         */
        Log.get().info(String.format("Servidor fonte = %s", fonte_hostname));
        Log.get().info(String.format("Servidor destino = %s", destino_hostname));
        //Log.get().info(String.format("Servidor adm fonte = %s", fonte_adm_hostname));
        //Log.get().info(String.format("Servidor adm destino = %s", destino_adm_hostname));

        map.put("main", new RemedyConnection());
    }

    public static RemedyConnection get() {
        String string = Thread.currentThread().getName();
        RemedyConnection c = map.getOrDefault(string, null);
        if (c == null) {
            c = new RemedyConnection();
            map.put(string, c);
        }
        return c;
    }
}
