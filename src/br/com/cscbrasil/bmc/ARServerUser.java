package br.com.cscbrasil.bmc;

import static br.com.cscbrasil.bmc.Statics.escapeForDatabase;
import com.bmc.arsys.api.ARErrors;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARNativeAuthenticationInfo;
import com.bmc.arsys.api.ActiveLink;
import com.bmc.arsys.api.ActiveLinkCriteria;
import com.bmc.arsys.api.AlertMessageCriteria;
import com.bmc.arsys.api.AlertMessageInfo;
import com.bmc.arsys.api.ArithmeticOrRelationalOperand;
import com.bmc.arsys.api.AssignInfo;
import com.bmc.arsys.api.Association;
import com.bmc.arsys.api.AssociationCriteria;
import com.bmc.arsys.api.BulkEntryReturn;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Container;
import com.bmc.arsys.api.ContainerCriteria;
import com.bmc.arsys.api.DiaryListValue;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.EntryListInfo;
import com.bmc.arsys.api.EntryPointInfo;
import com.bmc.arsys.api.EntryValueList;
import com.bmc.arsys.api.Escalation;
import com.bmc.arsys.api.EscalationCriteria;
import com.bmc.arsys.api.ExtFieldCandidatesInfo;
import com.bmc.arsys.api.ExtFormCandidatesInfo;
import com.bmc.arsys.api.ExtendedClassRegistry;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.FieldCriteria;
import com.bmc.arsys.api.Filter;
import com.bmc.arsys.api.FilterCriteria;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.FormAliasInfo;
import com.bmc.arsys.api.FormCriteria;
import com.bmc.arsys.api.GroupInfo;
import com.bmc.arsys.api.IARRowIterator;
import com.bmc.arsys.api.IARServerLogInterface;
import com.bmc.arsys.api.Image;
import com.bmc.arsys.api.ImageCriteria;
import com.bmc.arsys.api.LicenseInfo;
import com.bmc.arsys.api.LicenseValidInfo;
import com.bmc.arsys.api.LocalizedRequestInfo;
import com.bmc.arsys.api.LocalizedValueCriteria;
import com.bmc.arsys.api.LocalizedValueInfo;
import com.bmc.arsys.api.LoggingInfo;
import com.bmc.arsys.api.Menu;
import com.bmc.arsys.api.MenuCriteria;
import com.bmc.arsys.api.MenuItem;
import com.bmc.arsys.api.ObjectOperationTimes;
import com.bmc.arsys.api.ObjectPropertyMap;
import com.bmc.arsys.api.OutputInteger;
import com.bmc.arsys.api.OverlaidInfo;
import com.bmc.arsys.api.OverlayPropInfo;
import com.bmc.arsys.api.ProcessResult;
import com.bmc.arsys.api.Proxy;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.QuerySourceValues;
import com.bmc.arsys.api.RegularComplexQuery;
import com.bmc.arsys.api.RegularQuery;
import com.bmc.arsys.api.RoleInfo;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.ServerInfoMap;
import com.bmc.arsys.api.StatisticsResultInfo;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.arsys.api.SupportFile;
import com.bmc.arsys.api.SupportFileKey;
import com.bmc.arsys.api.Timestamp;
import com.bmc.arsys.api.UserInfo;
import com.bmc.arsys.api.ValidateFormCacheInfo;
import com.bmc.arsys.api.Value;
import com.bmc.arsys.api.View;
import com.bmc.arsys.api.ViewCriteria;
import com.bmc.arsys.api.WfdBreakpoint;
import com.bmc.arsys.api.WfdDebugLocation;
import com.bmc.arsys.api.WfdUserContext;
import com.bmc.arsys.api.WorkflowLockInfo;
import com.bmc.arsys.apitransport.ApiConfigI;
import com.bmc.arsys.apitransport.ApiPropertyManagerI;
import com.marzapower.loggable.Log;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ARServerUser extends com.bmc.arsys.api.ARServerUser {

    private String database;
    private String owner;
    private int maxRecords;

    public int cscGetMaxRecords() {
        return maxRecords;
    }

    public boolean cscIsOracle() {
        return database.toLowerCase().contains("oracle");
    }

    public String cscGetDatabase() {
        return database;
    }

    public String cscGetOwner() {
        return owner;
    }

    public void cscSetModeBest() {
        setBaseOverlayFlag(false);
        setOverlayFlag(true);
        setOverlayGroup("1");
        setDesignOverlayGroup("1");
    }

    public void cscSetModeBase() {
        setBaseOverlayFlag(true);
        setOverlayFlag(false);
        setOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
        setDesignOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
    }

    public int cscGetFormNumber(String formName) {
        SQLResult result;
        try {
            String cmd = String.format("select schemaid from arschema where name='%s' order by name,schemaid", Statics.escapeForDatabase(formName));
            result = getListSQL(cmd, 10, false);
        } catch (ARException ex) {
            Log.get().fatal(ex.getMessage(), ex);
            return 0;
        }

        List<List<Value>> records = result.getContents();
        if (records.isEmpty()) {
            return 0;
        }
        if (records.get(0).isEmpty()) {
            return 0;
        }
        Value v = records.get(0).get(0);
        if (v == null) {
            return 0;
        }
        if (v.getValue() instanceof Integer) {
            return v.getIntValue();
        }
        return 0;
    }

    public String cscGetCustomization(String formName) throws ARException {
        String customization;
        int overlayProp = Constants.AR_ORIGINAL_OBJECT;
        SQLResult sqlResult;
        sqlResult = getListSQL(String.format("select OVERLAYPROP,OVERLAYGROUP from ARSCHEMA where NAME='%s' order by OVERLAYPROP asc", escapeForDatabase(formName)), 999999999, false);
        List<List<Value>> results = sqlResult.getContents();
        for (List<Value> record : results) {
            overlayProp = record.get(0).getIntValue();
            break;
        }
        switch (overlayProp) {
            case Constants.AR_OVERLAID_OBJECT:
                customization = "Overlaid";
                break;
            case Constants.AR_OVERLAY_OBJECT:
                customization = "Overlay";
                break;
            case Constants.AR_CUSTOM_OBJECT:
                customization = "Custom";
                break;
            default:
                customization = "Unmodified";
                break;
        }
        return customization;
    }

    public String cscGetCustomization(Form form) throws ARException {
        return cscGetCustomization(form.getName());
    }

    public CmdbInfo cscGetCmdbInfo(String formName) {
        return new CmdbInfo(this, formName);
    }

    public int cscGetServerTime() throws ARException {
        int[] requestList = {Constants.AR_SERVER_INFO_SERVER_TIME};
        ServerInfoMap map = getServerInfo(requestList);
        return map.get(Constants.AR_SERVER_INFO_SERVER_TIME).getIntValue();
    }

    @Override
    public void login() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.login();
                database = "SQL -- Oracle";
                owner = "ARAdmin";
                int[] requestList = {Constants.AR_SERVER_INFO_DB_TYPE, Constants.AR_SERVER_INFO_DB_USER, Constants.AR_SERVER_INFO_MAX_ENTRIES};
                ServerInfoMap map;
                try {
                    map = getServerInfo(requestList);
                    database = map.getOrDefault(Constants.AR_SERVER_INFO_DB_TYPE, new Value("")).toString();
                    owner = map.getOrDefault(Constants.AR_SERVER_INFO_DB_USER, new Value("")).toString();
                    maxRecords = map.getOrDefault(Constants.AR_SERVER_INFO_MAX_ENTRIES, new Value("")).getIntValue();
                    if (maxRecords == 0) {
                        maxRecords = 999999999;
                    }
                } catch (ARException ex) {
                    Log.get().error(ex.getMessage());
                }
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    public ARServerUser(String user, String password, String locale, String server) {
        super(user, password, locale, server);
    }

    public ARServerUser(String user, String password, String authentication, String locale, String server) {
        super(user, password, authentication, locale, server);
    }

    public ARServerUser(String user, String password, String authentication, String locale, String timeZone, String customDateFormat, String customTimeFormat, String server) {
        super(user, password, authentication, locale, timeZone, customDateFormat, customTimeFormat, server);
    }

    public ARServerUser(String user, String password, String locale, String server, int serverPort) {
        super(user, password, locale, server, serverPort);
    }

    public ARServerUser() {
        super();
    }

    public ARServerUser(ARNativeAuthenticationInfo userInfo, String locale, String server) {
        super(userInfo, locale, server);
    }

    public ARServerUser(String user, String password, String authentication, String locale, String server, int serverPort) {
        super(user, password, authentication, locale, server, serverPort);
    }

    @Override
    public boolean equals(Object obj) {
        if (getClass() != obj.getClass()) {
            return false;
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    @Deprecated
    public void clear() {
        super.clear();
    }

    @Override
    public Field getField(String form, int fieldId, FieldCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getField(form, fieldId, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Field getField(String form, int fieldId) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getField(form, fieldId);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setForm(Form obj) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setForm(obj);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setForm(Form obj, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setForm(obj, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public int getPort() {
        return super.getPort();
    }

    @Override
    public Entry getEntry(String formName, String entryId, int[] entryListFields) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getEntry(formName, entryId, entryListFields);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Container getContainer(String name) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getContainer(name);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Container getContainer(String name, ContainerCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getContainer(name, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String getLocale() {
        return super.getLocale();
    }

    @Override
    public void setLocale(String locale) {
        super.setLocale(locale);
    }

    @Override
    public void setEntry(String formName, String entryId, Entry entry, Timestamp getTime, int nOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setEntry(formName, entryId, entry, getTime, nOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public String getTimeZone() {
        return super.getTimeZone();
    }

    @Override
    public void setTimeZone(String timeZone) {
        super.setTimeZone(timeZone);
    }

    @Override
    public void setPort(int serverPort) {
        super.setPort(serverPort);
    }

    @Override
    public void deleteEntry(String formName, String entryId, int deleteOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteEntry(formName, entryId, deleteOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setLastStatus(List lastStatus) {
        super.setLastStatus(lastStatus);
    }

    @Override
    public List<StatusInfo> getLastStatus() {
        return super.getLastStatus();
    }

    @Override
    public void logout() {
        super.logout();
    }

    @Override
    public Filter getFilter(String key, FilterCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getFilter(key, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Filter getFilter(String key) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getFilter(key);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void createImage(Image obj) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createImage(obj);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createImage(Image obj, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createImage(obj, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public ServerInfoMap getServerStatistics(int[] requestList) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getServerStatistics(requestList);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Menu> getListMenuObjects(long changedSince, List menus, MenuCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListMenuObjects(changedSince, menus, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Menu> getListMenuObjects(long changedSince, List forms, List activeLinks, MenuCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListMenuObjects(changedSince, forms, activeLinks, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public boolean isSubAdministrator() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.isSubAdministrator();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return false;
    }

    @Override
    public boolean isMemberOfBaseOverlayGroup() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.isMemberOfBaseOverlayGroup();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return false;
    }

    @Override
    public List<FormAliasInfo> getListFormAliases(long changedSince, int formType, String formName, int[] fieldIds, String vuiLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFormAliases(changedSince, formType, formName, fieldIds, vuiLabel);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<List<StatusInfo>> getMultiLastStatus() {
        return super.getMultiLastStatus();
    }

    @Override
    public String getCustomDateFormat() {
        return super.getCustomDateFormat();
    }

    @Override
    public void setNativeAuthenticationInfo(ARNativeAuthenticationInfo userInfo) {
        super.setNativeAuthenticationInfo(userInfo);
    }

    @Override
    public List<ExtFieldCandidatesInfo> getListViewFormFieldCandidates(String tableName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListViewFormFieldCandidates(tableName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListVendorForm(long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListVendorForm(changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<ExtFieldCandidatesInfo> getListVendorFormFieldCandidates(String vendorName, String tableName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListVendorFormFieldCandidates(vendorName, tableName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Container> getListContainerObjects(long changedSince, int[] containerTypes, boolean hiddenFlag, List ownerList, ObjectPropertyMap propsToSearch, ContainerCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListContainerObjects(changedSince, containerTypes, hiddenFlag, ownerList, propsToSearch, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Container> getListContainerObjects(List containers) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListContainerObjects(containers);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setMultiLastStatus(List statusInfo1, List statusInfo2) {
        super.setMultiLastStatus(statusInfo1, statusInfo2);
    }

    @Override
    public void setMultiLastStatus(List lastStatusList) {
        super.setMultiLastStatus(lastStatusList);
    }

    @Override
    public String getAuthentication() {
        return super.getAuthentication();
    }

    @Override
    public List<ExtFormCandidatesInfo> getListExtFormCandidates(int formType) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListExtFormCandidates(formType);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public boolean isMemberOfOverlayGroup() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.isMemberOfOverlayGroup();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return false;
    }

    @Override
    public List<String> getListDisplayOnlyForm(long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListDisplayOnlyForm(changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Form> getListFormObjects(long changedSince, int formType, String formName, int[] fieldIds, FormCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFormObjects(changedSince, formType, formName, fieldIds, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListRegularForm(long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListRegularForm(changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String getCustomTimeFormat() {
        return super.getCustomTimeFormat();
    }

    @Override
    public boolean isMemberOfCustomizeGroup() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.isMemberOfCustomizeGroup();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return false;
    }

    @Override
    public List<Field> getListFieldObjects(String formName, int fieldTypeMask) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFieldObjects(formName, fieldTypeMask);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Field> getListFieldObjects(String formName, int[] fieldIds, FieldCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFieldObjects(formName, fieldIds, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Field> getListFieldObjects(String formName, int fieldTypeMask, long changedSince, FieldCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFieldObjects(formName, fieldTypeMask, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Field> getListFieldObjects(String formName, int fieldTypeMask, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFieldObjects(formName, fieldTypeMask, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Field> getListFieldObjects(String formName, int[] fieldIds) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFieldObjects(formName, fieldIds);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Field> getListFieldObjects(String formName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFieldObjects(formName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public QualifierInfo parseQualification(String form, String qualification) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.parseQualification(form, qualification);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public QualifierInfo parseQualification(String queryString, List fieldList1, List fieldList2, int queryContext) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.parseQualification(queryString, fieldList1, fieldList2, queryContext);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public QualifierInfo parseQualification(String form, String vui, String qualification) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.parseQualification(form, vui, qualification);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public QualifierInfo parseQualification(String queryString, List fieldList1, List fieldList2, int queryContext, boolean exceptionWhenFieldNotFound) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.parseQualification(queryString, fieldList1, fieldList2, queryContext, exceptionWhenFieldNotFound);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setMultipleFields(List fields) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setMultipleFields(fields);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public String formatQualification(QualifierInfo qual, List fieldList1, List fieldList2, int queryContext, boolean isEscalation) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.formatQualification(qual, fieldList1, fieldList2, queryContext, isEscalation);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String formatQualification(String form, Object qualification) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.formatQualification(form, qualification);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<View> getListViewObjects(String formName, long changedSince, int[] viewIds, ViewCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListViewObjects(formName, changedSince, viewIds, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<View> getListViewObjects(String formName, long changedSince, ViewCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListViewObjects(formName, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public ProcessResult executeSpecialCommand(String cmd, Object[] parameters) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.executeSpecialCommand(cmd, parameters);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String xmlExecuteService(String queryMapping, String queryDoc, String inputMapping, String inputDoc, String outputMapping, String optionDoc) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.xmlExecuteService(queryMapping, queryDoc, inputMapping, inputDoc, outputMapping, optionDoc);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void createMultipleFields(List fields) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createMultipleFields(fields);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<String> getListEscalation(long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEscalation(changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListEscalation(String formName, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEscalation(formName, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListEscalation() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEscalation();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListEscalation(String formName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEscalation(formName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListEscalation(String formName, long changedSince, ObjectPropertyMap propsToSearch) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEscalation(formName, changedSince, propsToSearch);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Entry> getListEntryObjects(String formName, List entryIds, int[] fieldIds) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEntryObjects(formName, entryIds, fieldIds);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Entry> getListEntryObjects(String formName, QualifierInfo qualification, int firstRetrieve, int maxRetrieve, List sortList, int[] fieldIds, boolean useLocale, OutputInteger nMatches) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEntryObjects(formName, qualification, firstRetrieve, maxRetrieve, sortList, fieldIds, useLocale, nMatches);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void getListEntryObjects(String formName, QualifierInfo qualification, int firstRetrieve, int maxRetrieve, List sortList, int[] fieldIds, boolean useLocale, OutputInteger nMatches, IARRowIterator iterator) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.getListEntryObjects(formName, qualification, firstRetrieve, maxRetrieve, sortList, fieldIds, useLocale, nMatches, iterator);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<QuerySourceValues> getListEntryObjects(RegularQuery query, int firstRetrieve, int maxRetrieve, boolean useLocale, OutputInteger nMatches) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEntryObjects(query, firstRetrieve, maxRetrieve, useLocale, nMatches);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Escalation> getListEscalationObjects(String formName, long changedSince, EscalationCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEscalationObjects(formName, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Escalation> getListEscalationObjects(List names, long changedSince, EscalationCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEscalationObjects(names, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public ProcessResult executeProcessForActiveLink(String actlinkName, int actionIndex, int actionType, int fieldId, Timestamp timestamp, List keywordList, List parameterList) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.executeProcessForActiveLink(actlinkName, actionIndex, actionType, fieldId, timestamp, keywordList, parameterList);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public ProcessResult executeProcessForActiveLink(String actlinkName, int actionIndex, int actionType, int fieldId, Timestamp timestamp, List keywordList, List parameterList, boolean waitFlag) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.executeProcessForActiveLink(actlinkName, actionIndex, actionType, fieldId, timestamp, keywordList, parameterList, waitFlag);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<EntryPointInfo> getListEntryPoint(long changedSince, List applicationKeys, int[] refTypes, int viewType, String displayTag, boolean hiddenFlag) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEntryPoint(changedSince, applicationKeys, refTypes, viewType, displayTag, hiddenFlag);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Filter> getListFilterObjects() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilterObjects();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Filter> getListFilterObjects(List names) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilterObjects(names);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Filter> getListFilterObjects(List names, FilterCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilterObjects(names, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Filter> getListFilterObjects(List names, long changedSince, FilterCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilterObjects(names, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Filter> getListFilterObjects(String formName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilterObjects(formName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Filter> getListFilterObjects(String formName, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilterObjects(formName, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Filter> getListFilterObjects(String formName, long changedSince, FilterCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilterObjects(formName, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<EntryValueList> getListValuesFromEntries(String formName, QualifierInfo qualification, int startEntry, int maxEntries, List sortList, List selectionList, List groupByList, QualifierInfo having, boolean distinct, boolean useLocale, OutputInteger nMatches) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListValuesFromEntries(formName, qualification, startEntry, maxEntries, sortList, selectionList, groupByList, having, distinct, useLocale, nMatches);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListActiveLink(String formName, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLink(formName, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListActiveLink(String formName, long changedSince, ObjectPropertyMap propsToSearch) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLink(formName, changedSince, propsToSearch);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListActiveLink() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLink();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListActiveLink(String formName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLink(formName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListActiveLink(long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLink(changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Entry getOneEntryObject(String formName, QualifierInfo qualification, List sortList, int[] fieldIds, boolean useLocale, OutputInteger nMatches) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getOneEntryObject(formName, qualification, sortList, fieldIds, useLocale, nMatches);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public SQLResult getListSQLForActiveLink(String actlinkName, int actionIndex, int actionType, Timestamp timestamp, List keywordList, List parameterList, int maxRetrieve, boolean retrieveNumMatches) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListSQLForActiveLink(actlinkName, actionIndex, actionType, timestamp, keywordList, parameterList, maxRetrieve, retrieveNumMatches);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<ActiveLink> getListActiveLinkObjects(List names, long changedSince, ActiveLinkCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLinkObjects(names, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<ActiveLink> getListActiveLinkObjects(List names, ActiveLinkCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLinkObjects(names, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<ActiveLink> getListActiveLinkObjects(List names) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLinkObjects(names);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<ActiveLink> getListActiveLinkObjects(String formName, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLinkObjects(formName, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<ActiveLink> getListActiveLinkObjects(String formName, long changedSince, ActiveLinkCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLinkObjects(formName, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<ActiveLink> getListActiveLinkObjects(String formName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLinkObjects(formName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<ActiveLink> getListActiveLinkObjects() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListActiveLinkObjects();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setCustomDateFormat(String customDateFormat) {
        super.setCustomDateFormat(customDateFormat);
    }

    @Override
    public void setCustomTimeFormat(String customTimeFormat) {
        super.setCustomTimeFormat(customTimeFormat);
    }

    @Override
    public void setAuthentication(String authentication) {
        super.setAuthentication(authentication);
    }

    @Override
    public String getEndClientIPAddress() {
        return super.getEndClientIPAddress();
    }

    @Override
    public void setEndClientIPAddress(String endClientIPAddress) {
        super.setEndClientIPAddress(endClientIPAddress);
    }

    @Override
    public List<EntryValueList> getListValuesFromMultiSchemaEntries(RegularComplexQuery query, int firstRetrieve, int maxRetrieve, boolean useLocale, boolean selectDistinct, OutputInteger nMatches) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListValuesFromMultiSchemaEntries(query, firstRetrieve, maxRetrieve, useLocale, selectDistinct, nMatches);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void verifyUser() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.verifyUser();
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    @Deprecated
    public Proxy getProxy() {
        return super.getProxy();
    }

    @Override
    public String getServer() {
        return super.getServer();
    }

    @Override
    public void setOperationTime(Timestamp operationTime) {
        super.setOperationTime(operationTime);
    }

    @Override
    public void setUserContext(ARNativeAuthenticationInfo userInfo) {
        super.setUserContext(userInfo);
    }

    @Override
    public void setUser(String user) {
        super.setUser(user);
    }

    @Override
    public void setPassword(String password) {
        super.setPassword(password);
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public String getUser() {
        return super.getUser();
    }

    @Override
    @Deprecated
    public void setProxy(Proxy prxy) {
        super.setProxy(prxy);
    }

    @Override
    public void setServer(String server) {
        super.setServer(server);
    }

    @Override
    public void wfdSetDebugMode(int mode) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.wfdSetDebugMode(mode);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public QualifierInfo wfdGetFilterQual() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.wfdGetFilterQual();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void wfdTerminateAPI(int errorCode) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.wfdTerminateAPI(errorCode);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<Integer> getCacheEvent(int[] eventIdList, int returnOption, OutputInteger cacheCount) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getCacheEvent(eventIdList, returnOption, cacheCount);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListImage(List formList, long changedSince, String imageType, ObjectPropertyMap propsToSearch) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImage(formList, changedSince, imageType, propsToSearch);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListImage() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImage();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListImage(long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImage(changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListImage(List formList) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImage(formList);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListImage(String imageType) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImage(imageType);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListImage(List formList, long changedSince, String imageType) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImage(formList, changedSince, imageType);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void deleteAlert(String entryId) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteAlert(entryId);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void wfdSetBreakpoint(int errorCode, WfdBreakpoint bp) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.wfdSetBreakpoint(errorCode, bp);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setImage(Image obj) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setImage(obj);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setImage(Image obj, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setImage(obj, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public int[] wfdExecute(int mode) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.wfdExecute(mode);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public int wfdGetDebugMode() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.wfdGetDebugMode();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return 0;
    }

    @Override
    public void deleteImage(String imageName, boolean updateRef) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteImage(imageName, updateRef);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteImage(String imageName, boolean updateRef, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteImage(imageName, updateRef, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public AssignInfo decodeAssignment(String assignString) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.decodeAssignment(assignString);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public int getAlertCount(QualifierInfo qual) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getAlertCount(qual);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return 0;
    }

    @Override
    public String xmlCreateEntry(String inputMapping, String inputDoc, String outputMapping, String optionDoc) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.xmlCreateEntry(inputMapping, inputDoc, outputMapping, optionDoc);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String getLocalHostID() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getLocalHostID();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public String exportDef(List items, boolean asXml, String displayTag, int vuiType, WorkflowLockInfo lockInfo) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.exportDef(items, asXml, displayTag, vuiType, lockInfo);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public String exportDef(List items) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.exportDef(items);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public String exportDef(List items, boolean asXml) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.exportDef(items, asXml);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public String exportDef(List items, boolean asXml, WorkflowLockInfo lockInfo) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.exportDef(items, asXml, lockInfo);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public String exportDef(List items, WorkflowLockInfo lockInfo) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.exportDef(items, lockInfo);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public String exportDef(List items, String displayTag, int vuiType, WorkflowLockInfo lockInfo) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.exportDef(items, displayTag, vuiType, lockInfo);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void exportDefToFile(List items, boolean asXml, String defXmlFile, boolean overwrite) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.exportDefToFile(items, asXml, defXmlFile, overwrite);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void exportDefToFile(List items, boolean asXml, String defXmlFile, boolean overwrite, int expOption) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.exportDefToFile(items, asXml, defXmlFile, overwrite, expOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void exportDefToFile(List items, boolean asXml, WorkflowLockInfo lockInfo, String filePath, boolean overwrite) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.exportDefToFile(items, asXml, lockInfo, filePath, overwrite);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void exportDefToFile(List items, boolean asXml, WorkflowLockInfo lockInfo, String filePath, boolean overwrite, int expOption) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.exportDefToFile(items, asXml, lockInfo, filePath, overwrite, expOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void exportDefToFile(List items, boolean asXml, String displayTag, int vuiType, WorkflowLockInfo lockInfo, String filePath, boolean overwrite, int expOption) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.exportDefToFile(items, asXml, displayTag, vuiType, lockInfo, filePath, overwrite, expOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void exportDefToFile(List items, boolean asXml, String displayTag, int vuiType, WorkflowLockInfo lockInfo, String filePath, boolean overwrite) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.exportDefToFile(items, asXml, displayTag, vuiType, lockInfo, filePath, overwrite);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public String xmlSetEntry(String queryMapping, String queryDoc, String inputMapping, String inputDoc, String outputMapping, String optionDoc) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.xmlSetEntry(queryMapping, queryDoc, inputMapping, inputDoc, outputMapping, optionDoc);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String encodeAssignment(AssignInfo assignment) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.encodeAssignment(assignment);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String xmlGetEntry(String queryMapping, String queryDoc, String outputMapping, String optionDoc) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.xmlGetEntry(queryMapping, queryDoc, outputMapping, optionDoc);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public BigDecimal getCurrencyRatio(String currencyRatioSet, String fromCurrencyCode, String toCurrencyCode) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getCurrencyRatio(currencyRatioSet, fromCurrencyCode, toCurrencyCode);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String createAlertEvent(String user, String alertText, int priority, String sourceTag, String server, String formName, String objectId) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.createAlertEvent(user, alertText, priority, sourceTag, server, formName, objectId);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public int getVUIType() {
        return super.getVUIType();
    }

    @Override
    public int getTimeoutXLong() {
        return super.getTimeoutXLong();
    }

    @Override
    public int getTimeoutNormal() {
        return super.getTimeoutNormal();
    }

    @Override
    public void setTimeoutXLong(int timeoutXLong) {
        super.setTimeoutXLong(timeoutXLong);
    }

    @Override
    public void useAdminRpcQueue() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.useAdminRpcQueue();
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setTimeoutNormal(int timeoutNormal) {
        super.setTimeoutNormal(timeoutNormal);
    }

    @Override
    public void setVUIType(int vuiType) {
        super.setVUIType(vuiType);
    }

    @Override
    public void signalServer(Map signals) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.signalServer(signals);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public int getClientType() {
        return super.getClientType();
    }

    @Override
    public LicenseValidInfo validateLicense(String licenseType) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.validateLicense(licenseType);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public int getLastCount() {
        return super.getLastCount();
    }

    @Override
    public int getTimeoutLong() {
        return super.getTimeoutLong();
    }

    @Override
    public String getLastID() {
        return super.getLastID();
    }

    @Override
    public void setTimeoutLong(int timeoutLong) {
        super.setTimeoutLong(timeoutLong);
    }

    @Override
    public void setClientType(int clientType) {
        super.setClientType(clientType);
    }

    @Override
    public String generateGUID(String prefix) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.generateGUID(prefix);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String generateGUID() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.generateGUID();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public boolean isMemberOfGroup(int groupId) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.isMemberOfGroup(groupId);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return false;
    }

    @Override
    public Image getImage(String imageName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getImage(imageName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Image getImage(String imageName, ImageCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getImage(imageName, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Entry executeService(String formName, String entryId, Entry entry, int[] entryListFields) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.executeService(formName, entryId, entry, entryListFields);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String mergeEntry(String formName, Entry entry, int nMergeType, QualifierInfo qualification, int multimatchOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.mergeEntry(formName, entry, nMergeType, qualification, multimatchOption);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String mergeEntry(String formName, Entry entry, int nMergeType) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.mergeEntry(formName, entry, nMergeType);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<EntryListInfo> getListEntry(String formName, QualifierInfo qualification, int firstRetrieve, int maxRetrieve, List sortList, List entryListFields, boolean useLocale, OutputInteger nMatches) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListEntry(formName, qualification, firstRetrieve, maxRetrieve, sortList, entryListFields, useLocale, nMatches);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void getEntryBlob(String formName, String entryID, int fieldID, String filePath) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.getEntryBlob(formName, entryID, fieldID, filePath);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public byte[] getEntryBlob(String formName, String entryID, int fieldID) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getEntryBlob(formName, entryID, fieldID);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public ProcessResult executeProcess(String command, boolean waitFlag) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.executeProcess(command, waitFlag);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public ProcessResult executeProcess(String command) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.executeProcess(command);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public View getView(String formName, int viewId, ViewCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getView(formName, viewId, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public View getView(String formName, int viewId) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getView(formName, viewId);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void deleteView(String formName, int viewId) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteView(formName, viewId);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public AssignInfo parseAssignment(String assignString, String queryString, Field assignField, List fieldList1, List fieldList2, String form, String server, int currentScreen, boolean idOk, boolean fromSQL, boolean fromFilterAPI, int queryContext) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.parseAssignment(assignString, queryString, assignField, fieldList1, fieldList2, form, server, currentScreen, idOk, fromSQL, fromFilterAPI, queryContext);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public AssignInfo parseAssignment(String toForm, int fieldId, String fromForm, String expression) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.parseAssignment(toForm, fieldId, fromForm, expression);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String formatAssignment(String form, int fieldId, String form2, Object assignment) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.formatAssignment(form, fieldId, form2, assignment);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String formatAssignment(AssignInfo assign, List fieldList, Field assignField, boolean topLevel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.formatAssignment(assign, fieldList, assignField, topLevel);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void createView(View object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createView(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public DiaryListValue decodeDiary(String encoded) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.decodeDiary(encoded);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String createEntry(String formName, Entry entry) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.createEntry(formName, entry);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    public static String encodeDiary(DiaryListValue diaryList) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return com.bmc.arsys.api.ARServerUser.encodeDiary(diaryList);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setView(View object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setView(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<Integer> getListView(String formName, long changedSince, ObjectPropertyMap propsToSearch) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListView(formName, changedSince, propsToSearch);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Integer> getListView(String formName, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListView(formName, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Integer> getListField(String formName, int fieldTypeMask, long changedSince, ObjectPropertyMap propsToSearch) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListField(formName, fieldTypeMask, changedSince, propsToSearch);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Integer> getListField(String formName, int fieldTypeMask, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListField(formName, fieldTypeMask, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListJoinForm(long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListJoinForm(changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListViewForm(long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListViewForm(changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public String getVersionString() {
        return super.getVersionString();
    }

    @Override
    public void stopRecording() {
        super.stopRecording();
    }

    @Override
    public void createForm(Form obj) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createForm(obj);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createForm(Form obj, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createForm(obj, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteForm(String formName, int deleteOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteForm(formName, deleteOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteForm(String formName, int deleteOption, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteForm(formName, deleteOption, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public Form getForm(String formName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getForm(formName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Form getForm(String formName, FormCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getForm(formName, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public LoggingInfo getLogging() {
        return super.getLogging();
    }

    @Override
    public void setLogging(LoggingInfo info) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setLogging(info);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<String> startRecording(int recordingMode, String fileNamePrefix) {
        return super.startRecording(recordingMode, fileNamePrefix);
    }

    @Override
    public boolean isStructSubadmin() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.isStructSubadmin();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return false;
    }

    @Override
    public List<String> getListForm(long changedSince, int formType, String formName, int[] fieldIds, ObjectPropertyMap propsToSearch) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListForm(changedSince, formType, formName, fieldIds, propsToSearch);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListForm(long changedSince, int formType, String formName, int[] fieldIds) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListForm(changedSince, formType, formName, fieldIds);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListForm(long changedSince, int formType, String formName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListForm(changedSince, formType, formName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListForm() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListForm();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListForm(long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListForm(changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListForm(long changedSince, int formType) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListForm(changedSince, formType);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void deleteFields(String form, int[] fieldIds, int deleteOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteFields(form, fieldIds, deleteOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public String getServerVersion() {
        return super.getServerVersion();
    }

    @Override
    public boolean isStructAdmin() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.isStructAdmin();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return false;
    }

    @Override
    public void setLogInterface(IARServerLogInterface logInterface) {
        super.setLogInterface(logInterface);
    }

    @Override
    public void createContainer(Container object, boolean removeInvalidReference, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createContainer(object, removeInvalidReference, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createContainer(Container object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createContainer(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createContainer(Container object, boolean removeInvalidReference) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createContainer(object, removeInvalidReference);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteContainer(String name, int deleteOption, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteContainer(name, deleteOption, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteContainer(String name, int deleteOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteContainer(name, deleteOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<String> getListMenu(long changedSince, List forms, List activeLinks) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListMenu(changedSince, forms, activeLinks);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListMenu(long changedSince, List forms, List activeLinks, ObjectPropertyMap propsToSearch) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListMenu(changedSince, forms, activeLinks, propsToSearch);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setField(Field object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setField(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createField(Field object, boolean isReservedRangeIdOK) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createField(object, isReservedRangeIdOK);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteField(String form, int fieldId, int deleteOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteField(form, fieldId, deleteOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createMenu(Menu object, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createMenu(object, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createMenu(Menu object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createMenu(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setContainer(Container object, boolean removeInvalidReference) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setContainer(object, removeInvalidReference);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setContainer(Container object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setContainer(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setContainer(Container object, boolean removeInvalidReference, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setContainer(object, removeInvalidReference, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<MenuItem> expandMenu(Menu menu, Entry keywordList, Entry parameterList, int maxRetrieve, OutputInteger nMatches) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.expandMenu(menu, keywordList, parameterList, maxRetrieve, nMatches);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<MenuItem> expandMenu(Menu menu, Entry keywordList, Entry parameterList) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.expandMenu(menu, keywordList, parameterList);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<MenuItem> expandMenu(String menuName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.expandMenu(menuName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<MenuItem> expandMenu(String menuName, Entry keywordList, Entry parameterList) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.expandMenu(menuName, keywordList, parameterList);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<MenuItem> expandMenu(Menu menu) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.expandMenu(menu);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void deleteMenu(String name, int deleteOption, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteMenu(name, deleteOption, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteMenu(String name, int deleteOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteMenu(name, deleteOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<String> getListContainer(long changedSince, int[] containerTypes, boolean hiddenFlag, List ownerList, ObjectPropertyMap propsToSearch) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListContainer(changedSince, containerTypes, hiddenFlag, ownerList, propsToSearch);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Menu getMenu(String name, MenuCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getMenu(name, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setMenu(Menu object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setMenu(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setMenu(Menu object, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setMenu(object, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<Integer> getListSupportFile(int fileType, String associatedObjName, int supportingId, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListSupportFile(fileType, associatedObjName, supportingId, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getAllLocalHostIDs() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getAllLocalHostIDs();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<StatisticsResultInfo> getEntryStatistics(String formKey, QualifierInfo qual, ArithmeticOrRelationalOperand target, int statistic, int[] groupByList) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getEntryStatistics(formKey, qual, target, statistic, groupByList);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String encodeQualification(QualifierInfo qualifier) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.encodeQualification(qualifier);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public AlertMessageInfo decodeAlertMessage(AlertMessageCriteria criteria, Object message, int length) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.decodeAlertMessage(criteria, message, length);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String getTextForErrorMessage(int msgId) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getTextForErrorMessage(msgId);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public LocalizedValueInfo getLocalizedValue(LocalizedValueCriteria criteria, LocalizedRequestInfo request) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getLocalizedValue(criteria, request);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void deleteSupportFile(SupportFileKey supportFileKey) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteSupportFile(supportFileKey);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public int createSupportFile(SupportFile supportFile) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.createSupportFile(supportFile);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return 0;
    }

    @Override
    public List<String> getMultipleCurrencyRatioSets(List ratioTimestamps) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getMultipleCurrencyRatioSets(ratioTimestamps);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setApplicationState(String applicationName, String newState) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setApplicationState(applicationName, newState);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<BulkEntryReturn> endBulkEntryTransaction(int actionType) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.endBulkEntryTransaction(actionType);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<SupportFile> getListSupportFileObjects(int fileType, String associatedObjName, int supportingId, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListSupportFileObjects(fileType, associatedObjName, supportingId, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void registerForAlerts(int clientPort, int registrationFlags) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.registerForAlerts(clientPort, registrationFlags);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public QualifierInfo decodeQualification(String encodedQualText) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.decodeQualification(encodedQualText);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void deregisterForAlerts(int clientPort) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deregisterForAlerts(clientPort);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    @Deprecated
    public QualifierInfo decodeARQualifierStruct(String queryString) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.decodeARQualifierStruct(queryString);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public String encodeARAssignStruct(AssignInfo assign) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.encodeARAssignStruct(assign);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public String encodeARQualifierStruct(QualifierInfo qual) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.encodeARQualifierStruct(qual);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    @Deprecated
    public AssignInfo decodeARAssignStruct(String assignString) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.decodeARAssignStruct(assignString);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<LocalizedValueInfo> getMultipleLocalizedValues(LocalizedValueCriteria criteria, List requests) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getMultipleLocalizedValues(criteria, requests);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void beginBulkEntryTransaction() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.beginBulkEntryTransaction();
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<String> getListApplicationState() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListApplicationState();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String getApplicationState(String applicationName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getApplicationState(applicationName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<MenuItem> convertStringToListMenu(String menuString) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.convertStringToListMenu(menuString);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public int getServerVersionMinor() {
        return super.getServerVersionMinor();
    }

    @Override
    public boolean getOverridePrevIP() {
        return super.getOverridePrevIP();
    }

    @Override
    public int getChunkResponseSize() {
        return super.getChunkResponseSize();
    }

    @Override
    public void setOverridePrevIP(boolean overridePrevIP) {
        super.setOverridePrevIP(overridePrevIP);
    }

    @Override
    public void setDefaultOutputDirectory(String defaultOutputDirectory) {
        super.setDefaultOutputDirectory(defaultOutputDirectory);
    }

    @Override
    public AssignInfo decodeDSOPoolAssignment(String encodedStr) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.decodeDSOPoolAssignment(encodedStr);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public int getServerVersionMajor() {
        return super.getServerVersionMajor();
    }

    @Override
    @Deprecated
    public void importDefFromBuffer(String importBuffer, List items) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.importDefFromBuffer(importBuffer, items);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    @Deprecated
    public void importDefFromBuffer(String importBuffer, int optionMask, List items) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.importDefFromBuffer(importBuffer, optionMask, items);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    @Deprecated
    public void importDefFromBuffer(String importBuffer, int optionMask) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.importDefFromBuffer(importBuffer, optionMask);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    @Deprecated
    public void importDefFromBuffer(String importBuffer) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.importDefFromBuffer(importBuffer);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public String getDefaultOutputDirectory() {
        return super.getDefaultOutputDirectory();
    }

    @Override
    public List<LicenseValidInfo> validateMultipleLicense(List licenseTypes) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.validateMultipleLicense(licenseTypes);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public int getServerRpcQueueNumber() {
        return super.getServerRpcQueueNumber();
    }

    @Override
    public ExtendedClassRegistry getExtendedClassRegistry() {
        return super.getExtendedClassRegistry();
    }

    @Override
    public String encodeDSOPoolAssignment(AssignInfo asgnInfo) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.encodeDSOPoolAssignment(asgnInfo);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String getReservedParam1() {
        return super.getReservedParam1();
    }

    @Override
    public void setChunkResponseSize(int chunkResponseSize) {
        super.setChunkResponseSize(chunkResponseSize);
    }

    @Override
    public void setExtendedClassRegistry(ExtendedClassRegistry ecr) {
        super.setExtendedClassRegistry(ecr);
    }

    @Override
    public String getServerVersionPatch() {
        return super.getServerVersionPatch();
    }

    @Override
    public void useDefaultRpcQueue() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.useDefaultRpcQueue();
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setReservedParam1(String pParam) {
        super.setReservedParam1(pParam);
    }

    @Override
    public void usePrivateRpcQueue(int rpcProgramNum) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.usePrivateRpcQueue(rpcProgramNum);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void importDefFromFile(String filePath, int optionMask, List items, String objectModificationLogLabel) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.importDefFromFile(filePath, optionMask, items, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void importDefFromFile(String filePath, int optionMask, List items) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.importDefFromFile(filePath, optionMask, items);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void importDefFromFile(String filePath) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.importDefFromFile(filePath);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void importDefFromFile(String filePath, List items) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.importDefFromFile(filePath, items);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void importDefFromFile(String filePath, int optionMask) throws ARException, IOException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.importDefFromFile(filePath, optionMask);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public Value wfdGetKeywordValue(int kwId) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.wfdGetKeywordValue(kwId);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public WfdUserContext wfdGetUserContext(int mask) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.wfdGetUserContext(mask);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Image> getListImageObjects(List names, ImageCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImageObjects(names, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Image> getListImageObjects(String formName, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImageObjects(formName, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Image> getListImageObjects(List names, long changedSince, ImageCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImageObjects(names, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Image> getListImageObjects(String formName, long changedSince, ImageCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImageObjects(formName, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Image> getListImageObjects() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImageObjects();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Image> getListImageObjects(List names) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImageObjects(names);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Image> getListImageObjects(String formName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListImageObjects(formName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void removeClientManagedTransaction() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.removeClientManagedTransaction();
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setClientManagedTransaction(String transactionHandle) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setClientManagedTransaction(transactionHandle);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public Object parseFilterQualification(String form, String qualification) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.parseFilterQualification(form, qualification);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void endClientManagedTransaction(int transactionOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.endClientManagedTransaction(transactionOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void wfdSetFieldValues(List fieldValues) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.wfdSetFieldValues(fieldValues);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public HashMap<Integer, ObjectOperationTimes> getObjectChangeTimes() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getObjectChangeTimes();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String getTransactionHandle() {
        return super.getTransactionHandle();
    }

    @Override
    public String beginClientManagedTransaction() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.beginClientManagedTransaction();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String formatSetIfQualification(String form1, String form2, Object qualification) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.formatSetIfQualification(form1, form2, qualification);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Entry> wfdGetFieldValues(int depth) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.wfdGetFieldValues(depth);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public QualifierInfo parseSetIfQualification(String form1, String form2, String qualification) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.parseSetIfQualification(form1, form2, qualification);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public WfdDebugLocation wfdGetDebugLocation(int depth) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.wfdGetDebugLocation(depth);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void wfdSetQualifierResult(boolean result) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.wfdSetQualifierResult(result);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public ValidateFormCacheInfo getValidateFormCache(String form, Timestamp mostRecentActLink, Timestamp mostRecentMenu, Timestamp mostRecentGuide) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getValidateFormCache(form, mostRecentActLink, mostRecentMenu, mostRecentGuide);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Association> getListAssociationObjects(List names) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListAssociationObjects(names);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Association> getListAssociationObjects(List names, long changedSince, AssociationCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListAssociationObjects(names, changedSince, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<Association> getListAssociationObjects(List names, AssociationCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListAssociationObjects(names, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void deleteAssociation(String name, int deleteOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteAssociation(name, deleteOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteAssociation(String name, int deleteOption, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteAssociation(name, deleteOption, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public OverlayPropInfo createOverlayFromObject(OverlaidInfo baseObj, OverlaidInfo customObj) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.createOverlayFromObject(baseObj, customObj);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListAssociation(String formName, long changedSince, ObjectPropertyMap propsToSearch, int enforcement, int cardinality, int type) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListAssociation(formName, changedSince, propsToSearch, enforcement, cardinality, type);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public ApiPropertyManagerI getApiPropertyManager() {
        return super.getApiPropertyManager();
    }

    @Override
    public void wfdClearAllBreakpoints() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.wfdClearAllBreakpoints();
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void wfdClearBreakpoint(int bpId) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.wfdClearBreakpoint(bpId);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<WfdBreakpoint> wfdListBreakpoints() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.wfdListBreakpoints();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void createAssociation(Association object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createAssociation(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createAssociation(Association object, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createAssociation(object, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public boolean isAdministrator() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.isAdministrator();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return false;
    }

    @Override
    public void setActiveLink(ActiveLink object, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setActiveLink(object, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setActiveLink(ActiveLink object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setActiveLink(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<String> getListFilter() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilter();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListFilter(String formName, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilter(formName, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListFilter(String formName, long changedSince, ObjectPropertyMap propsToSearch) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilter(formName, changedSince, propsToSearch);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListFilter(String formName) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilter(formName);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListFilter(long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListFilter(changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setFilter(Filter object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setFilter(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setFilter(Filter object, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setFilter(object, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public Escalation getEscalation(String key) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getEscalation(key);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Escalation getEscalation(String key, EscalationCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getEscalation(key, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void createEscalation(Escalation object, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createEscalation(object, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createEscalation(Escalation object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createEscalation(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteEscalation(String name, int deleteOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteEscalation(name, deleteOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteEscalation(String name, int deleteOption, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteEscalation(name, deleteOption, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public ActiveLink getActiveLink(String name) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getActiveLink(name);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public ActiveLink getActiveLink(String name, ActiveLinkCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getActiveLink(name, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void createFilter(Filter object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createFilter(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createFilter(Filter object, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createFilter(object, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createActiveLink(ActiveLink object, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createActiveLink(object, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void createActiveLink(ActiveLink object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.createActiveLink(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public ServerInfoMap getServerInfo(int[] requestList) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getServerInfo(requestList);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setEscalation(Escalation object, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setEscalation(object, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setEscalation(Escalation object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setEscalation(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteFilter(String name, int deleteOption, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteFilter(name, deleteOption, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteFilter(String name, int deleteOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteFilter(name, deleteOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteActiveLink(String name, int deleteOption) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteActiveLink(name, deleteOption);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void deleteActiveLink(String name, int deleteOption, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.deleteActiveLink(name, deleteOption, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<GroupInfo> getListGroup(String user, String password) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListGroup(user, password);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public SupportFile getSupportFile(String associatedObjName, int fileId, String filePath) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getSupportFile(associatedObjName, fileId, filePath);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public SupportFile getSupportFile(String associatedObjName, int fileId, int fileType, int supportingId, String filePath) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getSupportFile(associatedObjName, fileId, fileType, supportingId, filePath);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public SupportFile getSupportFile(String associatedObjName, int fileId) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getSupportFile(associatedObjName, fileId);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public String getServerCharSet() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getServerCharSet();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<UserInfo> getListUser(int type, long changedSince) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListUser(type, changedSince);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<UserInfo> getListUser(int type) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListUser(type);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<UserInfo> getListUser() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListUser();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public List<String> getListAlertUser() throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListAlertUser();
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void impersonateUser(String impersonatedUser) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.impersonateUser(impersonatedUser);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public SQLResult getListSQL(String sqlCommand, int maxRetrieve, boolean retrieveNumMatches) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListSQL(sqlCommand, maxRetrieve, retrieveNumMatches);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setSupportFile(SupportFile supportFile) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setSupportFile(supportFile);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<RoleInfo> getListRole(String application, String user, String password) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListRole(application, user, password);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setServerInfo(ServerInfoMap serverInfoMap) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setServerInfo(serverInfoMap);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public List<LicenseInfo> getListLicense(String licenseType) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getListLicense(licenseType);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void setAssociation(Association object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setAssociation(object);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public void setAssociation(Association object, String objectModificationLogLabel) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.setAssociation(object, objectModificationLogLabel);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public Association getAssociation(String key) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getAssociation(key);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public Association getAssociation(String key, AssociationCriteria criteria) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.getAssociation(key, criteria);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void changeId(List changeIdInfoList) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.changeId(changeIdInfoList);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public ApiConfigI getApiConfig() {
        return super.getApiConfig();
    }

    @Override
    public OverlayPropInfo createOverlay(OverlaidInfo object) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.createOverlay(object);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

    @Override
    public void runEscalation(String escalation) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                super.runEscalation(escalation);
                break;
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
    }

    @Override
    public Entry setGetEntry(String formName, String entryId, Entry setEntry, Timestamp getTime, int nOption, int[] entryListFields) throws ARException {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                return super.setGetEntry(formName, entryId, setEntry, getTime, nOption, entryListFields);
            } catch (RuntimeException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    throw ex;
                }
            } catch (ARException ex) {
                List<StatusInfo> status = ex.getLastStatus();
                if (status.size() == 1) {
                    if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_CONNECT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_CALL
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_2
                            || status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT_3) {
                        Log.get().error(ex.getMessage());
                        if (status.get(0).getMessageNum() == ARErrors.AR_ERROR_RPC_TIMEOUT) {
                            break;
                        }
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex1) {
                            throw ex;
                        }
                        continue;
                    }
                }
                throw ex;
            }
        }
        return null;
    }

}
