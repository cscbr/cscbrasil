/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.bmc;

import com.bmc.thirdparty.org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author ivan
 */
public class Statics {

    public static String escapeForDatabase(String queryparam) {
        return StringEscapeUtils.escapeSql(queryparam);
    }
}
