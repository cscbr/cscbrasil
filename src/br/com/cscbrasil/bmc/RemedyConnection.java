/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.bmc;

import com.bmc.arsys.api.ARException;
import com.marzapower.loggable.Log;

/**
 *
 * @author ivan
 */
public class RemedyConnection {

    public ARServerUser sourc;
    public ARServerUser desti;

    private final String fonte_user;
    private final String fonte_password;
    private final String fonte_auth;
    private final String fonte_locale;
    private final String fonte_hostname;
    private final int fonte_port;
    private final int fonte_rpc;
    private final String desti_user;
    private final String desti_password;
    private final String desti_auth;
    private final String desti_locale;
    private final String desti_hostname;
    private final int desti_port;
    private final int desti_rpc;

    private String databaseFonte;
    private String databaseDesti;

    public String getSourceDatabase() {
        return databaseFonte;
    }

    public String getDestiDatabase() {
        return databaseFonte;
    }

    public RemedyConnection() {
        fonte_user = RemedyConnections.fonte_user;
        fonte_password = RemedyConnections.fonte_password;
        fonte_auth = RemedyConnections.fonte_auth;
        fonte_locale = RemedyConnections.fonte_locale;
        fonte_hostname = RemedyConnections.fonte_hostname;
        fonte_port = RemedyConnections.fonte_port;
        fonte_rpc = RemedyConnections.fonte_rpc;
        desti_user = RemedyConnections.destino_user;
        desti_password = RemedyConnections.destino_password;
        desti_auth = RemedyConnections.destino_auth;
        desti_locale = RemedyConnections.destino_locale;
        desti_hostname = RemedyConnections.destino_hostname;
        desti_port = RemedyConnections.destino_port;
        desti_rpc = RemedyConnections.destino_rpc;
        sourc = source_login();
        desti = desti_login();
    }

    private ARServerUser login(String user, String password, String auth, String locale, String hostname, int port, int rpc) {
        ARServerUser retorno = new ARServerUser(user, password, auth, locale, hostname, port);
        while (!Thread.currentThread().isInterrupted()) {
            try {
                retorno.usePrivateRpcQueue(rpc);
                retorno.login();
                retorno.setTimeoutLong(ARServerUserConstants.TIMEOUT_LONG);
                retorno.setTimeoutNormal(ARServerUserConstants.TIMEOUT_NORMAL);
                retorno.setTimeoutXLong(ARServerUserConstants.TIMEOUT_XLONG);
                return retorno;
            } catch (ARException ex) {
                Log.get().error(retorno.getServer() + " - " + ex.getMessage());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex1) {
                    Log.get().error(retorno.getServer() + " - " + ex1.getMessage());
                }
            }
        }
        return null;
    }

    private ARServerUser source_login() {
        return login(fonte_user, fonte_password, fonte_auth, fonte_locale, fonte_hostname, fonte_port, fonte_rpc);
    }

    private ARServerUser desti_login() {
        return login(desti_user, desti_password, desti_auth, desti_locale, desti_hostname, desti_port, desti_rpc);
    }
}
